import 'dart:convert';
import 'dart:io';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:skeleton_flutter/constants/api/api.dart';
import 'package:skeleton_flutter/data/response/api_common_res.dart';
import 'package:skeleton_flutter/data/response/source_to_destination.dart';
import 'package:skeleton_flutter/pages/base/base_controller.dart';
import 'package:dio/dio.dart' as dio;

class LocationDetailsController  extends BaseController{

  Rx<File> file = File("").obs;
  var isProgressUploadImage = false.obs;

  void setPickImage(String? path) {
    file.value = File(path ?? '');
  }

  void updateImage(SegmentLocation? segmentLocation) async {
    isProgressUploadImage.value = true;
    List<int> imageBytes = file.value.readAsBytesSync();
    String base64Image = "data:image/jpeg;base64,"+base64Encode(imageBytes);
    print("Image64 $base64Image");
    var formData = dio.FormData.fromMap({
      "AddressId": segmentLocation?.address_id ?? "",
      "ImageFile": base64Image,
    });
    var markerCompletedData = await callApi(
        api.post(url: LOCATION_IMAGE_UPLOAD, data: formData),
        onError: (e) {
          isProgressUploadImage.value = false;
          errorToast(e.printError());
          print("Resposne Error ${e.printError()}");
        });
    print("Resposne $markerCompletedData");
    if (markerCompletedData != null) {
      isProgressUploadImage.value = false;
      var apiResModel = ApiResModel.fromJson(markerCompletedData);
      //I/flutter (28091): 200 : {status: true, image: https://agostinofoods.000webhostapp.com/uploads/1638931907.jpeg}
      segmentLocation?.setImage(apiResModel.image ?? "");
      //successToast("Image Uploaded Success");
      Get.back(result: segmentLocation);
    }
  }

  void makeCompletedThisMarker(SegmentLocation? segmentLocation, String isCompleted) async {
    var formData = dio.FormData.fromMap({
      "AddressId": segmentLocation?.address_id ?? "",
      "isCompleted": ""+isCompleted,
    });
    var markerCompletedData = await callApi(
        api.post(url: MARK_COMPLETED, data: formData),
        onError: (e) {});
    if (markerCompletedData != null) {
      var apiResModel = ApiResModel.fromJson(markerCompletedData);
      segmentLocation!.setCompleted(isCompleted);
      segmentLocation.setSkip(isCompleted == "0");
      Get.back(result: segmentLocation);
    }
  }
}