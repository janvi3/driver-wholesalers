import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:image_picker/image_picker.dart';
import 'package:skeleton_flutter/constants/api/api.dart';
import 'package:skeleton_flutter/constants/styles/common_style.dart';
import 'package:skeleton_flutter/data/response/source_to_destination.dart';
import 'package:skeleton_flutter/gen/assets.gen.dart';
import 'package:skeleton_flutter/gen/colors.gen.dart';

import 'location_controller.dart';

class LocationDetails extends StatefulWidget {
  final SegmentLocation segmentLocation;

  const LocationDetails({Key? key, required this.segmentLocation})
      : super(key: key);

  @override
  _LocationDetailsState createState() => _LocationDetailsState();
}

class _LocationDetailsState extends State<LocationDetails> {
  final LocationDetailsController _controller =
      Get.put(LocationDetailsController());
  final _imagePicker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorName.greyBg,
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            "location".tr,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 24.sp),
          ),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(16.h),
          child: Column(
            children: [
              Container(
                height: 100.h,
                width: double.infinity,
                decoration: getContainerBg(),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16.w),
                  child: Image.network(
                      'https://maps.googleapis.com/maps/api/staticmap?center=${widget.segmentLocation.lat},${widget.segmentLocation.lng}&zoom=14&size=400x200&markers=color:red%7Clabel:D%7C${widget.segmentLocation.lat},${widget.segmentLocation.lng}&key=$GoogleAPIKey',
                      fit: BoxFit.cover),
                ),
              ),
              _verticalSpace(),
              Container(
                  decoration: getContainerBg(),
                  padding: EdgeInsets.all(16.w),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image(
                        image:
                            AssetImage(Assets.res.drawables.icPinRed.assetName),
                        width: 30,
                        height: 30,
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${widget.segmentLocation.address}",
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700),
                            ),
                            SizedBox(
                              height: 10.h,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20)),
                                  color: ColorName.pinkColor),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 16.0,
                                    right: 16.0,
                                    top: 4.0,
                                    bottom: 4.0),
                                child: Text(
                                  widget.segmentLocation.completed == "1"
                                      ? "Completed"
                                      : "Not Completed",
                                  style: TextStyle(
                                      fontSize: 12.sp,
                                      color: widget.segmentLocation.completed ==
                                              "1"
                                          ? Colors.green
                                          : Colors.red),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  )),
              _verticalSpace(),
              Container(
                  decoration: getContainerBg(),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          children: [
                            Expanded(
                                child: Text("notes".tr,
                                    style: TextStyle(
                                        fontSize: 16.sp,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w700))),
                            Text("added_by_admin".tr,
                                style: TextStyle(
                                    fontSize: 12.sp, color: Colors.grey[400])),
                          ],
                        ),
                        Text(
                            "${widget.segmentLocation.notes != null ? widget.segmentLocation.notes : "-"}",
                            style: TextStyle(
                                fontSize: 14.sp, color: Colors.grey[400])),
                        SizedBox(
                          height: 16.h,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text("Publications",
                                    style: TextStyle(
                                        fontSize: 16.sp,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w700))),
                            Text("${widget.segmentLocation.publications}",
                                style: TextStyle(
                                    fontSize: 12.sp, color: Colors.grey[400])),
                          ],
                        ),
                      ],
                    ),
                  )),
              _verticalSpace(),
              Container(
                  decoration: getContainerBg(),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("capture_photo".tr,
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w700)),
                            Text("you_can_take_photo_".tr,
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: Colors.grey[400],
                                    fontStyle: FontStyle.italic)),
                            Obx(() => _controller.isProgressUploadImage.value
                                ? SizedBox(
                                    width: 35.r,
                                    height: 35.r,
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          ColorName.accentColor),
                                      strokeWidth: 5.w,
                                    ))
                                : ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: ColorName.splashBgColor,
                                      onPrimary: Colors.white,
                                      shadowColor: Colors.greenAccent,
                                      elevation: 3,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                    ),
                                    onPressed: () {
                                      if (_controller.file.value.path != "") {
                                        _controller.updateImage(
                                            widget.segmentLocation);
                                      }
                                    },
                                    child: Text(
                                      "Upload",
                                    ),
                                  ))
                          ],
                        )),
                        SizedBox(
                          width: 10.w,
                        ),
                        InkWell(
                          child: Obx(() => _controller.file.value.path != "" || widget.segmentLocation.image?.isEmpty == false
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: _controller.file.value.path != ""
                                      ? Image.file(
                                          _controller.file.value,
                                          fit: BoxFit.cover,
                                          height: 100.h,
                                          width: 80.h,
                                        )
                                      : Image.network(
                                          "${widget.segmentLocation.image}",
                                          fit: BoxFit.cover,
                                          height: 100.h,
                                          width: 80.h,
                                        ),
                                )
                              : DottedBorder(
                                  color: Colors.black,
                                  strokeWidth: 1,
                                  borderType: BorderType.RRect,
                                  radius: Radius.circular(10),
                                  child: Container(
                                    height: 100.h,
                                    margin: EdgeInsets.all(8.0),
                                    child: Padding(
                                      padding: EdgeInsets.all(4.w),
                                      child: Center(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Image(
                                              image: AssetImage(Assets
                                                  .res
                                                  .drawables
                                                  .icCamera
                                                  .assetName),
                                              width: 40,
                                              height: 40,
                                            ),
                                            SizedBox(
                                              height: 10.h,
                                            ),
                                            Text("click_here_to_capture".tr,
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    color: Colors.grey[400])),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                )),
                          onTap: () {
                            _imgFromGallery();
                          },
                        ),
                      ],
                    ),
                  )),
              _verticalSpace(),
              _verticalSpace(),
              _bottomButtons(),
            ],
          ),
        ));
  }

  Widget _verticalSpace() {
    return SizedBox(
      height: 20.h,
    );
  }

  _imgFromGallery() async {
    final pickedFile = await _imagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 75);
    _controller.setPickImage(pickedFile?.path);
  }

  Widget _bottomButtons() {
    return Obx(() => (_controller.isProgressShowing.value &&
            !_controller.isProgressUploadImage.value)
        ? _showProgress()
        : Column(
            children: [
              widget.segmentLocation.completed == "1"
                  ? Container()
                  : ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: ColorName.splashBgColor,
                        onPrimary: Colors.white,
                        shadowColor: Colors.greenAccent,
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        minimumSize: Size(double.infinity, 50), //////// HERE
                      ),
                      onPressed: () {
                        _controller.makeCompletedThisMarker(
                            widget.segmentLocation, "1");
                      },
                      child: Text(
                        "Mark as a Completed",
                      ),
                    ),
              SizedBox(
                height: 10.h,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  onPrimary: Colors.black,
                  shadowColor: Colors.grey,
                  elevation: 3,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(color: Colors.grey)),
                  minimumSize: Size(double.infinity, 50), //////// HERE
                ),
                onPressed: () {
                  _controller.makeCompletedThisMarker(
                      widget.segmentLocation, "0");
                },
                child: Text(
                  "skip_this_location".tr,
                ),
              ),
            ],
          ));
  }

  _showProgress() {
    return Center(
      child: SizedBox(
          width: 35.r,
          height: 35.r,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(ColorName.accentColor),
            strokeWidth: 5.w,
          )),
    );
  }
}
