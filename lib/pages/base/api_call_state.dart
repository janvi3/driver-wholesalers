import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part '../../gen/api_call_state.freezed.dart';

@freezed
abstract class ApiCallState<T> with _$ApiCallState {
  const factory ApiCallState.idle() = Idle;
  const factory ApiCallState.success(T response, int reqCode) = Success;
  const factory ApiCallState.loading() = Loading;
  const factory ApiCallState.error(String message) = Error;
}