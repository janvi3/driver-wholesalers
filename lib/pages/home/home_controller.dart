import 'dart:async';
import 'dart:math';

import 'package:dio/dio.dart' as dio;
import 'package:fl_location/fl_location.dart' as geoLocation;
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geofence_service/geofence_service.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:skeleton_flutter/constants/api/api.dart';
import 'package:skeleton_flutter/data/response/api_common_res.dart';
import 'package:skeleton_flutter/data/response/source_to_destination.dart';
import 'package:skeleton_flutter/pages/base/base_controller.dart';
import 'package:skeleton_flutter/pages/location/location_details.dart';
import 'package:skeleton_flutter/pages/login/login.dart';
import 'package:skeleton_flutter/pages/segment/segment_list.dart';
import 'package:wakelock/wakelock.dart';

class HomeController extends BaseController {
  List<SegmentLocation> listOfSegmentLocation = [];

  Map<String, Marker> markers = {};
  Map<String, Marker> currentMarkers = {};
  var maxSpeed = "50";
  var REGION_GEOFENCE = 50.0;
  var isProgressMap = false.obs;
  var userTouchMap = false.obs;

  // Map storing polylines created by connecting two points
  Map<PolylineId, Polyline> polylines = {};

  // the user's initial location and current location// as it moves
  late LocationData currentLocation;
  //LocationData? lastLocation;

  StreamController<MapRefreshType> isMapRefresh =
      StreamController<MapRefreshType>();

  var SOURCE = "source";

  List<LatLng> listOfAddress = [
    LatLng(23.430685302704997, 73.95394744466506),
    LatLng(23.431119019270923, 73.94997239112854),
    LatLng(23.431158395757976, 73.94835233688354),
    LatLng(23.435517786034612, 73.94339709662427),
    LatLng(23.437041268239266, 73.94062833864541),
    LatLng(23.442481691367508, 73.93481989512553)
  ];

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();

  final _geofenceService = GeofenceService.instance.setup(
      interval: 5000,
      accuracy: 100,
      loiteringDelayMs: 60000,
      statusChangeDelayMs: 10000,
      useActivityRecognition: true,
      allowMockLocations: false,
      printDevLog: false,
      geofenceRadiusSortType: GeofenceRadiusSortType.DESC);

  @override
  void onInit() {
    super.onInit();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _geofenceService
          .addGeofenceStatusChangeListener(_onGeofenceStatusChanged);
      //_geofenceService.addLocationChangeListener(_onLocationChanged);
      //_geofenceService.addLocationServicesStatusChangeListener(_onLocationServicesStatusChanged);
      //_geofenceService.addActivityChangeListener(_onActivityChanged);
      //_geofenceService.addStreamErrorListener(_onError);
      //_geofenceService.start(_geofenceList).catchError(_onError);
    });
    _getMaxSpeed();
  }

  final List<LatLng> allPolylineCoordinates = [];

  refreshMapForPoly() {
    isProgressMap.value = false;
    isMapRefresh.add(MapRefreshType.REFRESH);
  }

  getUserLatLngFromServer(dynamic arguments) async {
    var latLngData = await callApi(
        api.get(
            url: GET_SEGMENT_LOCATION +
                "/" +
                arguments[0] /*storage.getUserId()!*/,
            isCompleteUrl: true), onError: (e) {
      //state(ApiCallState.error("failed"));
    });
    if (latLngData != null) {
      isProgressMap.value = true;
      var apiResModel = ApiResModel.fromJson(latLngData);
      var sourceDestinationData =
          SourceDestinationData.fromJson(apiResModel.data);

      if (sourceDestinationData.listoflatlng != null) {
        ///Sort Logic Below
        sourceDestinationData.listoflatlng!.sort((a, b) {
          return calculateDistance(
                  currentLocation.latitude!,
                  currentLocation.longitude!,
                  double.parse(a.lat ?? "0"),
                  double.parse(a.lng ?? "0"))
              .compareTo(calculateDistance(
                  currentLocation.latitude!,
                  currentLocation.longitude!,
                  double.parse(b.lat ?? "0"),
                  double.parse(b.lng ?? "0")));
        });
        for (var source in sourceDestinationData.listoflatlng!) {
          print("Sort By ${source.address}");
        }

        listOfSegmentLocation = sourceDestinationData.listoflatlng!;
        for (var source in sourceDestinationData.listoflatlng!) {
          //First Add Marker
          //print("Count ${source.toString()}");
          var latlng =
              LatLng(double.parse(source.lat!), double.parse(source.lng!));
          //Add Marker Here
          await addMarker(latlng, "" + source.address_id!, "" + source.address!,
              source.completed == "1");
          //Add Geofence Data Here
          await _addGeoFenceLocation(latlng, source.address_id ?? "");
        }
        //await _addGeoFenceLocation(LatLng(23.11860997992094, 72.56953969816084),  "Home");
        //await _addGeoFenceLocation(LatLng(23.120046, 72.569471), "Cross");
        //await _addGeoFenceLocation(LatLng(23.120035789429163, 72.56780699126863),  "Hotel");
        //return;
        int i = 0;
        /**
         * Add Our Current Location Route in 0 location
         */
        sourceDestinationData.listoflatlng?.insert(
          0,
          SegmentLocation(
              lat: currentLocation.latitude.toString(),
              lng: currentLocation.longitude.toString(),
              address: "Your Location"),
        );

        ///Using Waypount below code
        /*var sourceLocation = sourceDestinationData.listoflatlng![0];
        var destinationLocation = sourceDestinationData
            .listoflatlng!.last;

        final polylinePoints = PolylinePoints();

        final startPoint = PointLatLng(double.parse(sourceLocation.lat!),
            double.parse(sourceLocation.lng!));
        final finishPoint = PointLatLng(double.parse(destinationLocation.lat!),
            double.parse(destinationLocation.lng!));

        List<PolylineWayPoint> wayPoint = [];

        wayPoint.add(
            PolylineWayPoint(location: "23.117704053824657,72.56898610438537"));
        wayPoint.add(PolylineWayPoint(location: "23.117934,72.57240969999999"));
        wayPoint.add(PolylineWayPoint(location: "23.1182011,72.5743887"));
        wayPoint.add(PolylineWayPoint(location: "23.1174234,72.5750178"));
        wayPoint.add(
            PolylineWayPoint(location: "23.112976262178833,72.57634511129761"));

        final result = await polylinePoints.getRouteBetweenCoordinates(
            GoogleAPIKey, startPoint, finishPoint,
            optimizeWaypoints: true, wayPoints: wayPoint);
        if (result.points.isNotEmpty) {
          // loop through all PointLatLng points and convert them
          // to a list of LatLng, required by the Polyline
          result.points.forEach((PointLatLng point) {
            allPolylineCoordinates.add(
              LatLng(point.latitude, point.longitude),
            );
          });
        }
        print("All Poly Points  =>>>>> ${allPolylineCoordinates.length}");
        _geofenceService.start();
        refreshMapForPoly();
        return;*/

        print("List oF Size Loc ${sourceDestinationData.listoflatlng?.length}");

        while (i < sourceDestinationData.listoflatlng!.length - 1) {
          final polylinePoints = PolylinePoints();
          // Holds each polyline coordinate as Lat and Lng pairs
          var sourceData = sourceDestinationData.listoflatlng![i];
          var destinationData = sourceDestinationData.listoflatlng![i + 1];

          final startPoint = PointLatLng(
              double.parse(sourceData.lat!), double.parse(sourceData.lng!));
          final finishPoint = PointLatLng(double.parse(destinationData.lat!),
              double.parse(destinationData.lng!));

          final result = await polylinePoints.getRouteBetweenCoordinates(
              GoogleAPIKey, startPoint, finishPoint,
              optimizeWaypoints: true);
          if (result.points.isNotEmpty) {
            // loop through all PointLatLng points and convert them
            // to a list of LatLng, required by the Polyline
            result.points.forEach((PointLatLng point) {
              allPolylineCoordinates.add(
                LatLng(point.latitude, point.longitude),
              );
            });
          }
          i++;
        }
      }
      _geofenceService.start();
      refreshMapForPoly();
      Wakelock.enable();
    }
  }

  Future<void> addMarker(
      LatLng latLng, String id, String title, bool isCompleted) async {
    //If complted than show green marker otherwise red
    final bitmapIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(75, 75)),
        isCompleted
            ? 'res/drawables/ic_pin_green.png'
            : 'res/drawables/ic_pin_red.png');
    markers[id] = Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(id),
        position: latLng,
        infoWindow: InfoWindow(
          title: '' + title,
        ),
        icon: bitmapIcon,
        onTap: () {
          var segmentLocation = _getSegmentLocation(id);
          if (segmentLocation != null)
            Get.to(LocationDetails(segmentLocation: segmentLocation))
                ?.then((value) => {_handleLocationDetailChanges(value)});
        });
  }

  Future<void> addCurrentMarker(LatLng latLng) async {
    final bitmapIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(80, 80)),
        'res/drawables/ic_pin_source.png');
    var marker = Marker(
      // This marker id can be anything that uniquely identifies each marker.
      markerId: MarkerId(SOURCE),
      position: latLng,
      infoWindow: InfoWindow(
        title: 'Your Location',
      ),
      icon: bitmapIcon,
    );
    //markers[SOURCE] = marker;
    currentMarkers[SOURCE] = marker;
  }

  Future<Polyline> getRoutePolyline(
      {required LatLng start,
      required LatLng finish,
      required String id,
      Color color = Colors.blue,
      int width = 6}) async {
    // Generates every polyline between start and finish
    final polylinePoints = PolylinePoints();
    // Holds each polyline coordinate as Lat and Lng pairs
    final List<LatLng> polylineCoordinates = [];

    final startPoint = PointLatLng(start.latitude, start.longitude);
    final finishPoint = PointLatLng(finish.latitude, finish.longitude);

    final result = await polylinePoints.getRouteBetweenCoordinates(
      GoogleAPIKey,
      startPoint,
      finishPoint,
    );
    if (result.points.isNotEmpty) {
      // loop through all PointLatLng points and convert them
      // to a list of LatLng, required by the Polyline
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(
          LatLng(point.latitude, point.longitude),
        );
      });
    }
    //Add For later use
    allPolylineCoordinates.addAll(polylineCoordinates);
    final polyline = Polyline(
      polylineId: PolylineId(id),
      color: color,
      points: allPolylineCoordinates,
      width: width,
    );

    return polyline;
  }

  void logoutDialog() {
    Get.dialog(
      AlertDialog(
        title: Text("logout".tr),
        content: Text("are_you_sure_logout_".tr),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(
              "no".tr,
            ),
          ),
          TextButton(
            onPressed: () {
              _logout();
            },
            child: Text(
              "logout".tr,
            ),
          ),
        ],
      ),
      barrierDismissible: true,
    );
  }

  _logout() async {
    await storage.clear();
    Get.offAll(LoginScreen());
  }

  _getMaxSpeed() async {
    var maxSpeedData = await callApi(api.get(url: GET_MAX_SPEED));
    if (maxSpeedData != null) {
      var apiResModel = ApiResModel.fromJson(maxSpeedData);
      if (apiResModel.status == true) {
        maxSpeed = (apiResModel.data as Map)["speed"].toString();
      }
    }
  }

  void reportSpeedViolation(speed) async {
    var formData = dio.FormData.fromMap({
      "DriverId": storage.getUserId(),
      "Speed": speed,
      "Latitude": currentLocation.latitude,
      "Longitude": currentLocation.longitude,
    });
    var speedViolationData = await callApi(api.post(
        url: REPORT_SPEED_VIOLATION,
        data:
            formData)); //remove onError if error should be handled automatically (i.e. toast)
    if (speedViolationData != null) {
      var apiResModel = ApiResModel.fromJson(speedViolationData);
      if (apiResModel.status == true) {
        //Violation Send Succ
      }
    }
  }

  _addGeoFenceLocation(LatLng latLng, String addressID) async {
    var geofenceData = Geofence(
      id: addressID,
      latitude: latLng.latitude,
      longitude: latLng.longitude,
      radius: [
        //GeofenceRadius(id: 'radius_100m', length: 100),
        GeofenceRadius(id: 'radius_50m', length: 50),
        //GeofenceRadius(id: 'radius_25m', length: 25),
        // GeofenceRadius(id: 'radius_250m', length: 250),
        // GeofenceRadius(id: 'radius_200m', length: 200),
      ],
    );
    _geofenceService.addGeofence(geofenceData);
  }

  /// Cancel All the data here
  void cancelAll() async {
    isMapRefresh.close();
    _geofenceService.clearGeofenceList();
    _geofenceService.stop();
    Wakelock.disable();
  }

  _makeMarkerGreen(Geofence entry) async {
    final bitmapIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(75, 75)),
        'res/drawables/ic_pin_green.png');

    //If last location match than show the completed dialog
    var destinationLocation =
        listOfSegmentLocation[listOfSegmentLocation.length - 1];
    if (destinationLocation.address_id == entry.id) {
      //Remove All The geofence and Show the complete Dialog
      //Geofence.removeAllGeolocations();
      _geofenceService.clearGeofenceList();
      _showCompleteJourneyDialog();
    }
    var segmentLocation = _getSegmentLocation(entry.id);
    segmentLocation?.setCompleted("1");
    markers[entry.id] = Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(entry.id),
        position: LatLng(entry.latitude, entry.longitude),
        infoWindow: InfoWindow(
          title: segmentLocation?.address ?? "",
        ),
        icon: bitmapIcon,
        onTap: () {
          if (segmentLocation != null)
            Get.to(LocationDetails(segmentLocation: segmentLocation));
        });

    ///Update in global list aswell
    if (segmentLocation != null)
      listOfSegmentLocation[listOfSegmentLocation.indexWhere(
          (element) => element.address_id == entry.id)] = segmentLocation;

    ///Make API Call and Completed
    _makeCompletedThisMarker(entry.id);
    _removeGeofenceLocation(entry);
    refreshMapForPoly();
  }

  _removeGeofenceLocation(Geofence entry) async {
    _geofenceService.removeGeofence(entry);
  }

  void _makeCompletedThisMarker(addressId) async {
    var formData = dio.FormData.fromMap({
      "AddressId": addressId,
      "ImageFile": "",
      "isCompleted": "1",
    });
    var markerCompletedData = await callApi(
        api.post(url: MARK_COMPLETED, data: formData),
        onError: (e) {});
    if (markerCompletedData != null) {
      var apiResModel = ApiResModel.fromJson(markerCompletedData);
    }
  }

  SegmentLocation? _getSegmentLocation(id) {
    return listOfSegmentLocation
        .singleWhere((element) => element.address_id == id);
  }

  _handleLocationDetailChanges(value) async {
    var segLocation = value as SegmentLocation;
    //Update List with Updated Value
    listOfSegmentLocation[listOfSegmentLocation.indexWhere(
            (element) => element.address_id == segLocation.address_id)] =
        segLocation;

    //Update Marker Also
    final bitmapIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(75, 75)),
        segLocation.completed == "1"
            ? 'res/drawables/ic_pin_green.png'
            : 'res/drawables/ic_pin_red.png');

    markers[segLocation.address_id ?? ""] = Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(segLocation.address_id ?? ""),
        position: LatLng(double.parse(segLocation.lat ?? "0"),
            double.parse(segLocation.lng ?? "0")),
        infoWindow: InfoWindow(
          title: segLocation.address,
        ),
        icon: bitmapIcon,
        onTap: () {
          if (segLocation != null)
            Get.to(LocationDetails(segmentLocation: segLocation));
        });
    refreshMapForPoly();

    /*if (segLocation.completed == "1") {
      //Competed
      successToast("Completed");
    } else {
      //Skip Here
      successToast("Skip");
    }*/
  }

  void _showCompleteJourneyDialog() {
    Get.dialog(
      AlertDialog(
        title: Text("Completed"),
        content: Text("You have completed all the Locations"),
        actions: <Widget>[
          /*TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(
              "no".tr,
            ),
          ),*/
          TextButton(
            onPressed: () {
              Get.off(SegmentListScreen());
            },
            child: Text(
              "Back",
            ),
          ),
        ],
      ),
      barrierDismissible: false,
    );
  }

  // This function is to be called when the geofence status is changed.
  Future<void> _onGeofenceStatusChanged(
      Geofence geofence,
      GeofenceRadius geofenceRadius,
      GeofenceStatus geofenceStatus,
      geoLocation.Location location) async {
    print('geofence: ${geofence.toJson()}');
    print('geofenceRadius: ${geofenceRadius.toJson()}');
    print('geofenceStatus: ${geofenceStatus.toString()}');
    if (geofenceStatus == GeofenceStatus.ENTER) {
      _makeMarkerGreen(geofence);
      //scheduleNotification("Enter ${geofence.id}", "${geofence.toJson()}");
    }
    if (geofenceStatus == GeofenceStatus.EXIT) {
      //scheduleNotification("EXIT", "${geofence.toJson()}");
    }
    //_geofenceStreamController.sink.add(geofence);
  }

  getGeofenceService() => _geofenceService;

  void scheduleNotification(String title, String subtitle) {
    print("scheduling one with $title and $subtitle");
    var rng = new Random();
    Future.delayed(Duration(seconds: 2)).then((result) async {
      var androidPlatformChannelSpecifics = AndroidNotificationDetails(
          'your channel id', 'your channel name',
          importance: Importance.high,
          priority: Priority.high,
          ticker: 'ticker');
      var iOSPlatformChannelSpecifics = IOSNotificationDetails();
      var platformChannelSpecifics = NotificationDetails(
          android: androidPlatformChannelSpecifics,
          iOS: iOSPlatformChannelSpecifics);
      await flutterLocalNotificationsPlugin.show(
          rng.nextInt(100000), title, subtitle, platformChannelSpecifics,
          payload: 'item x');
    });
  }

  /// Result is in Meter
  /// if you want meter just multiply by 1000. return 1000 * 12742 * asin(sqrt(a))
  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 1000 * 12742 * asin(sqrt(a));
  }
}

enum MapRefreshType {
  REFRESH,
  INFO_WINDOW_SHOWN,
  RECENTER,
}
