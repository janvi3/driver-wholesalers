import 'package:flutter/gestures.dart';

class TouchHandle extends EagerGestureRecognizer {
  Function _test;

  TouchHandle(this._test);

  @override
  void resolve(GestureDisposition disposition) {
    super.resolve(disposition);
    this._test();
  }
}