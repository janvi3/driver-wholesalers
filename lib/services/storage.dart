import 'dart:convert';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skeleton_flutter/constants/api/storage.dart';
import 'package:skeleton_flutter/data/model/login_model.dart';
import 'dio.dart';

class StorageService {
  final _box = GetStorage();

  bool hasData(String key) => _box.hasData(key);

  setAuthToken(String authToken) {
    _box.write(AUTH_TOKEN, authToken);
    Get.find<DioService>().setAuthToken(authToken);
  }

  String? getAuthToken() => _box.read(AUTH_TOKEN);

  setUserInfo(LoginModel loginModel) {
    _box.write(USER_INFO, json.encode(loginModel.toJson()));
  }

  setUserID(String userId) {
    _box.write(USER_ID, userId);
  }

  String? getUserId() => _box.read(USER_ID);

  LoginModel getUserInfo() =>
      LoginModel.fromJson(json.decode(_box.read(USER_INFO)));

  clear() async => await _box.erase();

  remove(String key) async => await _box.remove(key);
}
