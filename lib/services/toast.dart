import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ToastService {
  errorToast(String message) {
    _showSnackbar(message, Colors.redAccent);
  }

  successToast(String message) {
    _showSnackbar(message, Colors.greenAccent);
  }

  _showSnackbar(String message, Color color) {
    Get.rawSnackbar(
        backgroundColor: color,
        snackStyle: SnackStyle.FLOATING,
        snackPosition: SnackPosition.TOP,
        messageText: Text(
          message,
          style: TextStyle(color: Colors.white, fontSize: 20.sp),
        ));
  }
}
