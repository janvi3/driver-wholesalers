import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skeleton_flutter/ioc/locator.dart';
import 'package:skeleton_flutter/pages/init_page.dart';
import 'package:skeleton_flutter/strings/strings.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  Locator.loadDependencies();
  if (Platform.isIOS) {
    await Firebase.initializeApp(
        options: FirebaseOptions(
            apiKey: "AIzaSyAHAsf51D0A407EklG1bs-5wA7EbyfNFg0",
          appId: '1:448618578101:ios:0b11ed8263232715ac3efc',
          messagingSenderId: '448618578101',
          projectId: 'react-native-firebase-testing',));
  } else {
    await Firebase.initializeApp();
  }
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      enableLog: false,
      localizationsDelegates: [
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      translations: Strings(),
      locale: Locale("en", "US"),
      supportedLocales: [Locale("en", "US"), Locale("ar", "AR")],
      theme: ThemeData(
          primaryColor: Colors.white,
          appBarTheme: AppBarTheme(
              iconTheme: IconThemeData(color: Colors.black),
              backgroundColor: Colors.white),
          bottomSheetTheme: BottomSheetThemeData(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  topRight: Radius.circular(10.0)),
            ),
          ),
          fontFamily: 'Nunito'),
      home: InitPage(),
    );
  }
}
