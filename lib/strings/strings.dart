import 'package:get/get.dart';

class Strings extends Translations {

  static var hello = "hello".tr;


  //Option 1
  // static var nameX = "hello"; // invoke: Strings.nameX.trArgs(["smoke"])

  //Option 2, Recommended
  static nameX(String name){  // invoke: Strings.nameX("smoke")
    return "name_x".trArgs([name]);
  }

  //Option 3
  // static nameX(List<String> name){  // invoke: Strings.nameX(["smoke"])
  //   return "name_x".trArgs(name);
  // }

  @override
  Map<String, Map<String, String>> get keys => {
    'en_US': {
      'hello' : 'Hello',
      'e_enter_email': 'Please Enter Email address',
      'e_invalid_email': 'Please Enter Valid Email address',
      'e_enter_password': 'Please Enter Password',
      'notification': 'Notification',
      'home': 'Home',
      'logout': 'Logout',
      'location': 'Location',
      'location_comp': 'Location Completed',
      'location_remain': 'Location Remaining',
      'start_direction': 'Start Direction',
      'skip_this_location': 'Skip this Location',
      'capture_photo': 'Capture Photo',
      'added_by_admin': 'Added By admin',
      'notes': 'Notes',
      'no': 'No',
      'ok': 'Ok',
      'are_you_sure_logout_': 'Are you sure you want to logout?',
      'your_email_pass_are_wrong': 'Your email and password are wrong!',
      'click_here_to_capture': 'Click here to\ncapture photo',
      'you_can_take_photo_': 'You can take photo by click on the button set as proof of delivery',
      'segment': 'Select Route',
      'reroute': 'Select Route',
    },
    'ar_AR': {
      'hello': 'Hallo Welt',
      'name_x': 'اسم %s',
    }
  };
}