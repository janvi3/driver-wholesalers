
/// notifications : [{"Title":"Pick up","Message":"Returns","ReceivedOn":"2021-11-14 06:08:52"}]

class NotificationData {
  NotificationData({
      List<Notifications>? notifications,}){
    _notifications = notifications;
}

  NotificationData.fromJson(dynamic json) {
    if (json['notifications'] != null) {
      _notifications = [];
      json['notifications'].forEach((v) {
        _notifications?.add(Notifications.fromJson(v));
      });
    }
  }
  List<Notifications>? _notifications;

  List<Notifications>? get notifications => _notifications;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_notifications != null) {
      map['notifications'] = _notifications?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// Title : "Pick up"
/// Message : "Returns"
/// ReceivedOn : "2021-11-14 06:08:52"

class Notifications {
  Notifications({
      String? title, 
      String? message, 
      String? receivedOn,}){
    _title = title;
    _message = message;
    _receivedOn = receivedOn;
}

  Notifications.fromJson(dynamic json) {
    _title = json['Title'];
    _message = json['Message'];
    _receivedOn = json['ReceivedOn'];
  }
  String? _title;
  String? _message;
  String? _receivedOn;

  String? get title => _title;
  String? get message => _message;
  String? get receivedOn => _receivedOn;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Title'] = _title;
    map['Message'] = _message;
    map['ReceivedOn'] = _receivedOn;
    return map;
  }

}