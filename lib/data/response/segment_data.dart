class SegmentData {
  SegmentData({
      List<Segments>? segments,}){
    _segments = segments;
}

  SegmentData.fromJson(dynamic json) {
    if (json['segments'] != null) {
      _segments = [];
      json['segments'].forEach((v) {
        _segments?.add(Segments.fromJson(v));
      });
    }
  }
  List<Segments>? _segments;

  List<Segments>? get segments => _segments;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_segments != null) {
      map['segments'] = _segments?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// SegmentId : "1"
/// SegmentTitle : "2021-Nov-01-153854"

class Segments {
  Segments({
      String? segmentId, 
      String? segmentTitle,}){
    _segmentId = segmentId;
    _segmentTitle = segmentTitle;
}

  Segments.fromJson(dynamic json) {
    _segmentId = json['SegmentId'];
    _segmentTitle = json['SegmentTitle'];
  }
  String? _segmentId;
  String? _segmentTitle;

  String? get segmentId => _segmentId;
  String? get segmentTitle => _segmentTitle;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['SegmentId'] = _segmentId;
    map['SegmentTitle'] = _segmentTitle;
    return map;
  }

}