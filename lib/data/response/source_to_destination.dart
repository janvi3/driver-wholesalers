/// status : true
/// data : {"listoflatlng":[{"lat":"20.5215151","lng":"73.61515151","address":"adresss kdf ask fsjd f"},{"lat":"20.5215151","lng":"73.61515151","address":"adresss kdf ask fsjd f"},{"lat":"20.5215151","lng":"73.61515151","address":"adresss kdf ask fsjd f"},null]}

/// listoflatlng : [{"lat":"20.5215151","lng":"73.61515151","address":"adresss kdf ask fsjd f"},{"lat":"20.5215151","lng":"73.61515151","address":"adresss kdf ask fsjd f"},{"lat":"20.5215151","lng":"73.61515151","address":"adresss kdf ask fsjd f"},null]

class SourceDestinationData {
  SourceDestinationData({
    List<SegmentLocation>? listoflatlng,
  }) {
    _listoflatlng = listoflatlng;
  }

  SourceDestinationData.fromJson(dynamic json) {
    if (json['listoflatlng'] != null) {
      _listoflatlng = [];
      json['listoflatlng'].forEach((v) {
        _listoflatlng?.add(SegmentLocation.fromJson(v));
      });
    }
  }

  List<SegmentLocation>? _listoflatlng;

  List<SegmentLocation>? get listoflatlng => _listoflatlng;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_listoflatlng != null) {
      map['listoflatlng'] = _listoflatlng?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// lat : "20.5215151"
/// lng : "73.61515151"
/// address : "adresss kdf ask fsjd f"

class SegmentLocation {
  SegmentLocation({
    String? lat,
    String? lng,
    String? address,
    String? addressId,
    String? completed,
    String? notes,
    String? image,
    String? publications,
    bool? isSkiped,
  }) {
    _lat = lat;
    _lng = lng;
    _address = address;
    _address_id = addressId;
    _completed = completed;
    _notes = notes;
    _image = image;
    _publications = publications;
    _isSkiped = isSkiped;
  }

  SegmentLocation.fromJson(dynamic json) {
    _lat = json['lat'];
    _lng = json['Lng'];
    _address = json['address'];
    _address_id = json['AddressId'];
    _completed = json['completed'];
    _notes = json['notes'];
    _image = json['img'];
    _publications = json['publications'];
    _isSkiped = json['isSkiped'];
  }

  String? _lat;
  String? _lng;
  String? _address;
  String? _address_id;
  String? _completed;
  String? _notes;
  String? _image;
  String? _publications;
  bool? _isSkiped;

  String? get lat => _lat;

  String? get lng => _lng;

  String? get address => _address;

  String? get address_id => _address_id;

  String? get completed => _completed;

  String? get notes => _notes;

  String? get image => _image;

  String? get publications => _publications;

  bool? get isSkiped => _isSkiped;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['lat'] = _lat;
    map['lng'] = _lng;
    map['address'] = _address;
    map['AddressId'] = _address_id;
    map['completed'] = _completed;
    map['notes'] = _notes;
    map['img'] = _image;
    map['publications'] = _publications;
    map['isSkiped'] = _isSkiped;
    return map;
  }

  void setCompleted(String isComp) {
    _completed = isComp;
  }

  void setImage(String url) {
    _image = url;
  }

  void setSkip(bool isSkip) {
    _isSkiped = isSkip;
  }
}
